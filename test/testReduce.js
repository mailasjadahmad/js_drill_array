const reduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5];

console.log(
  reduce(
    items,
    (acc, element, index, elements) => {
      acc.push(element);

      return acc;
    },
    []
  )
);
