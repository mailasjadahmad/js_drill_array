const filter=require("../filter")
const items = [1, 2, 3, 4, 5, 5];

console.log(
  filter(items, (element, index, array) => {
    return element % 2 == 0;
  })
);