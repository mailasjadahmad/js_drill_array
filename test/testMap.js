const map = require("../map");
const items = [1, 2, 3, 4, 5, 5];
const result = map(items, (element, index, array) => {
  return element * index;
});

console.log(result);
