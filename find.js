function find(elements, cb) {
  if (Array.isArray(elements)) {
    if (typeof cb === "function") {
      for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements)) {
          return elements[index];
        }
      }
      return undefined;
    } else {
      console.warn("enter a valid function");
    }
  } else {
    console.warn("enter a valid array");
  }
}


module.exports = find;
