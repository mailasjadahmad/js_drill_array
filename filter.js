function filter(elements, cb) {
  if (Array.isArray(elements)) {
    if (typeof cb === "function") {
      const newArr = [];
      for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements)) {
          newArr.push(elements[index]);
        }
      }
      return newArr;
    } else {
      console.warn("enter a valid function");
    }
  } else {
    console.warn("enter a valid array");
  }
}

module.exports = filter;
