function reduce(elements, cb, startingValue) {
  if (Array.isArray(elements)) {
    if (typeof cb === "function") {
      let acc = startingValue;
      for (let index = 0; index < elements.length; index++) {
        cb(acc, elements[index], index, elements);
      }
      return acc;
    } else {
      console.warn("enter a valid function");
    }
  } else {
    console.warn("enter a valid array");
  }
}

module.exports=reduce