function each(elements, cb) {
  if (Array.isArray(elements)) {
    if (typeof cb === "function") {
      for (let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
      }
    } else {
      console.warn("enter a valid function");
    }
  } else {
    console.warn("enter a valid array");
  }
}
module.exports=each;


