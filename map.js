function map(elements, cb) {
  if (Array.isArray(elements)) {
    if (typeof cb === "function") {
      const newEl = [];
      for (let index = 0; index < elements.length; index++) {
        newEl[index] = cb(elements[index], index, elements);
      }
      return newEl;
    } else {
      console.warn("enter a valid function");
    }
  } else {
    console.warn("enter a valid array");
  }
};


module.exports=map;